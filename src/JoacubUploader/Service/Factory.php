<?php

namespace JoacubUploader\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use JoacubUploader\Manager;

/**
 * FileBank service manager factory
 */
class Factory implements FactoryInterface 
{
    /**
     * Factory method for FileBank Manager service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return \JoacubUploader\Manager
     */
    public function createService(ServiceLocatorInterface $serviceLocator) {
        $manager = new Manager($serviceLocator);
        return $manager;
    }
}